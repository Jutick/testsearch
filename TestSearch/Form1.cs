﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace TestSearch
{
	delegate void Del(string s);

	public partial class Form1 : Form
	{
		// Найденные файлы 
		private static int count = 0;

		// Cчетчик файлов 
		private static TextBox textFileCount = new TextBox { Location = new System.Drawing.Point(305, 344), Enabled = false, Width = 50 };

		// Текущий файла 
		private static TextBox textCurrentFile = new TextBox { Location = new System.Drawing.Point(280, 308), Enabled = false,  Width = 310 };

		//Время выполнения 
		private static TextBox textTime = new TextBox { Location = new System.Drawing.Point(540, 344), Enabled = false, Width = 50 };

		// Вывод результатов в виде дерева
		private static TreeView treeResult = new TreeView() { Location = new System.Drawing.Point(0, 0), Height = 450, Width = 160 };

		// Таймер
		private System.Windows.Forms.Timer timer1 = new System.Windows.Forms.Timer { Interval = 1000};

		// Показания счетчика
		private static int sec, min;

		//Создание нового потока с поиском
		private Thread searchThread = new Thread(new ParameterizedThreadStart(Search));

		//Сериализатор для сохранения состояния при перезапуске программы
		private XmlSerializer save = new XmlSerializer(typeof(SearchInfo));

		public Form1()
		{
			InitializeComponent();

			this.Controls.Add(textFileCount);
			this.Controls.Add(textCurrentFile);
			this.Controls.Add(treeResult);
			this.Controls.Add(textTime);

			timer1.Tick += new EventHandler(timer1_Tick);
			timer1.Interval = 1000;
			//timer1.Stop();

			// Заполнение формы прошлым запросом
			if (System.IO.File.Exists("save.xml"))
			{
				using (FileStream fs = new FileStream("save.xml", FileMode.OpenOrCreate))
				{
					var info = (SearchInfo)save.Deserialize(fs);

					textDirectory.Text = info.Directory;
					textName.Text = info.Name;
					textText.Text = info.Text;
				}
			}
		}

		private void buttonStart_Click(object sender, EventArgs e)
		{
			try
			{
				if (textText.Text == "" || textName.Text == "" ||textDirectory.Text == "")
				{
					throw new Exception("Заполните все поля");
				}

				buttonStop.Enabled = true;
				//Сброс значений с прошлой итерации
				sec = 0;
				min = 0;
				count = 0;
				textFileCount.Text = count.ToString();
				treeResult.Nodes.Clear();

				// Значение полей введенных в форму
				var info = new SearchInfo(textDirectory.Text, textName.Text, textText.Text);

				// Cохранение полей
				using (FileStream fs = new FileStream("save.xml", FileMode.Create))
				{
					save.Serialize(fs, info);
				}

				if (searchThread.ThreadState == ThreadState.Suspended)
				{
					searchThread.Resume();
					searchThread.Abort();
				}

				//Создание нового потока с поиском
				searchThread = new Thread(new ParameterizedThreadStart(Search));

				//Запуск таймера
				textTime.Text = "< 1s";

				timer1.Enabled = true;
				//timer1.Start();
				

				//Запуск поиска
				searchThread.Start(info);

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message,"Ошибка");
			}
		}

		private static void Search(object obinf)
		{
			// Получаем критерии для поиска
			SearchInfo info = (SearchInfo)obinf;

			try
			{
				//Поиск папок в выбранной директории
				foreach (var f in Directory.GetDirectories(info.Directory))
				{
					//Запуск поиска еще раз для найденных папок 
					Search(new SearchInfo(f, info.Name, info.Text));

				}

				// Посик файлов 
				foreach (var f in Directory.GetFiles(info.Directory))
				{
					//Увеличение счетчика найденных файлов 
					textFileCount.Invoke(new Action(() => count++));
					
					// Вывод счетчик
					textFileCount.Invoke(new Del((s) => textFileCount.Text = s), count.ToString());

					// Вывод текущий файл
					textCurrentFile.Invoke(new Del((s) => textCurrentFile.Text = s), Path.GetFileName(f));

					//Регулярки для поиска 
					Regex nameRegEx = new Regex(info.Name, RegexOptions.IgnoreCase);
					Regex textRegEx = new Regex(info.Text, RegexOptions.IgnoreCase);

					// Определение кодировки
					StreamReader str = new StreamReader(f, true);

					// Проверка имени и текста и вывод в дерево (unicode, ANSU, ASCII) 
					if ((nameRegEx.IsMatch(Path.GetFileName(f))) && (File.ReadAllText(f, str.CurrentEncoding).Contains(info.Text))
						|| (nameRegEx.IsMatch(Path.GetFileName(f))) && (File.ReadAllText(f, Encoding.GetEncoding(1251)).Contains(info.Text))
						|| (nameRegEx.IsMatch(Path.GetFileName(f))) && (File.ReadAllText(f, Encoding.ASCII).Contains(info.Text)))
					{

						string[] dirNodes = f.Split(new char[] { '\\' });


						TreeNode fNode = new TreeNode();
						TreeNode[] mNode = new TreeNode[1];
						TreeNode sNode = new TreeNode();


						// Создание дерева в первый раз
						if (treeResult.Nodes.Count == 0)
						{
							for (int i = dirNodes.Length; i > 0; i--)
							{
								if (mNode[0] == null)
								{
									fNode = new TreeNode(dirNodes[i - 1]);
									fNode.Name = dirNodes[i - 1];
									mNode[0] = fNode;
								}
								else
								{
									fNode = new TreeNode(dirNodes[i - 1], mNode);
									fNode.Name = dirNodes[i - 1];
									mNode[0] = fNode;
								}
							}
							treeResult.Invoke(new Action(() => treeResult.Nodes.Add(fNode)));
						}

						// Добавление ветвей
						if (treeResult.Nodes.ContainsKey(dirNodes[0]))
						{
							//Начало ветви
							sNode = treeResult.Nodes[dirNodes[0]];
							for (int j = 0; j < dirNodes.Length - 1; j++)
							{
								//Если нижний уровень директории есть в ветви, то переход к следующему узлу
								if (sNode.Nodes.ContainsKey(dirNodes[j + 1]))
								{
									sNode = sNode.Nodes[dirNodes[j + 1]];
								}
								//Если нет создание дочерний узел 
								else if (sNode.Nodes != null)
								{
									treeResult.Invoke(new Action(() => sNode.Nodes.Add(dirNodes[j + 1]).Name = dirNodes[j + 1]));
									sNode = sNode.Nodes[dirNodes[j + 1]];
								}
							}
							
						}

					}
				}
			}
			catch(Exception e)
			{
				//MessageBox.Show(e.Message);
			}

		}

		/// <summary>
		/// Выбор дериктории
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void buttonDirectorySelect_Click(object sender, EventArgs e)
		{
			var fbd = new FolderBrowserDialog();
			fbd.Description = "Выберите нужную директорию";
			if (fbd.ShowDialog() == DialogResult.OK)
			{
				textDirectory.Text = fbd.SelectedPath;
			}
		}

		/// <summary>
		/// Сериализация данных в полях при выходе 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Form1_FormClosed(object sender, FormClosedEventArgs e)
		{
			//Значение полей введенных в форму
			var info = new SearchInfo
			{
				Directory = textDirectory.Text,
				Name = textName.Text,
				Text = textText.Text
			};

			using (FileStream fs = new FileStream("save.xml", FileMode.Create))
			{
				save.Serialize(fs, info);
			}

			if  (searchThread.ThreadState==ThreadState.Suspended)
			{
				searchThread.Resume();
				searchThread.Abort();
			}
		}

		private void buttonStop_Click(object sender, EventArgs e)
		{

			if (searchThread.IsAlive)
			{
				searchThread.Suspend();
				timer1.Stop();
				buttonResume.Enabled = true;
			}
			
		}

		private void buttonResume_Click(object sender, EventArgs e)
		{
			if (searchThread.IsAlive)
			{
				searchThread.Resume();
				timer1.Start();
				buttonResume.Enabled = false;
			}
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			if (searchThread.IsAlive)
			{
				sec++;
				if (sec == 60)
				{ sec = 0; min++; }

				string time = String.Format("{0:00}:{1:00}", min, sec);

				//textTime.Text = time;
				textTime.Invoke(new Del((s) => textTime.Text = s), time);
			}
			else
			{
				timer1.Stop();
				timer1.Dispose();
				buttonResume.Enabled = false;
				buttonStop.Enabled = false;
			}
		}
	}
}
