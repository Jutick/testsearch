﻿using System;


namespace TestSearch
{
	/// <summary>
	/// Класс с информацией о поиске
	/// </summary>
	[Serializable]
	public class SearchInfo
	{
		public SearchInfo() { }
		public SearchInfo(string dir, string n, string t)
		{
			 Directory = dir;
			 Name = n;
			 Text = t;
		}

		public string Directory { get; set; }

		public string Name { get; set; }

		public string Text { get; set; }
	}

}
